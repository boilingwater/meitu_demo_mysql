# Build docker image
sudo docker build -t docker-registry.cirrus:5000/mysql .

# Push docker image
sudo docker push docker-registry.cirrus:5000/mysql

# Start an instance
sudo docker run --name mysql -e MYSQL_ROOT_PASSWORD=mypass -d docker-registry.cirrus:5000/mysql
